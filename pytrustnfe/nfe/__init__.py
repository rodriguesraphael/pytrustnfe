# -*- coding: utf-8 -*-
# © 2016 Danimar Ribeiro, Trustcode
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import base64
import os
import hashlib

from lxml import etree
from .assinatura import Assinatura
from pytrustnfe.xml import render_xml, sanitize_response
from pytrustnfe.utils import gerar_chave, ChaveNFe
from pytrustnfe.Servidores import localizar_url, localizar_url_nfce, localizar_qrcode
from pytrustnfe.certificado import extract_cert_and_key_from_pfx, save_cert_key

# Zeep
from requests import Session
from zeep import Client
from zeep.transports import Transport


def _generate_nfe_id(**kwargs):
    for item in kwargs['NFes']:
        vals = {
            'cnpj': item['infNFe']['emit']['cnpj_cpf'],
            'estado': item['infNFe']['ide']['cUF'],
            'emissao': '%s%s' % (item['infNFe']['ide']['dhEmi'][2:4],
                                 item['infNFe']['ide']['dhEmi'][5:7]),
            'modelo': item['infNFe']['ide']['mod'],
            'serie': item['infNFe']['ide']['serie'],
            'numero': item['infNFe']['ide']['nNF'],
            'tipo': item['infNFe']['ide']['tpEmis'],
            'codigo': item['infNFe']['ide']['cNF'],
        }
        chave_nfe = ChaveNFe(**vals)
        chave_nfe = gerar_chave(chave_nfe, 'NFe')
        item['infNFe']['Id'] = chave_nfe
        item['infNFe']['ide']['cDV'] = chave_nfe[len(chave_nfe) - 1:]


def generate_qr_code(xml, **kwargs):
    """
    Método para geração do QR Code 2.00
    :param xml: xml da NFC-e
    :param kwargs: csc, id_token_csc
    :return: QR Code e URL para consulta
    """
    xml_string = etree.fromstring(xml)
    inf_nfe = kwargs['NFes'][0]['infNFe']
    nfe = xml_string.find('.//{http://www.portalfiscal.inf.br/nfe}NFe')
    inf_nfe_supl = etree.Element('infNFeSupl')
    chave_nfe = inf_nfe['Id'][3:]
    qr_code_versao = '2'
    ambiente = kwargs['ambiente']
    estado = kwargs['estado']
    tipo_emissao = str(inf_nfe['ide']['tpEmis'])
    dia_emissao = inf_nfe['ide']['dhEmi'][8:10]
    valor_total = inf_nfe['total']['vNF']
    dig_val = xml_string.find(".//{http://www.w3.org/2000/09/xmldsig#}DigestValue").text.encode()
    dig_val = base64.b16encode(dig_val).decode().lower()  # Don't change
    # CID Token
    cid_token = kwargs['NFes'][0]['infNFe']['codigo_seguranca']['cid_token']
    # Código de Segurança do Contribuinte
    csc = kwargs['NFes'][0]['infNFe']['codigo_seguranca']['csc']
    qr_code_server = localizar_qrcode(estado, ambiente)
    url_chave = localizar_url_nfce(estado, ambiente)

    if tipo_emissao != '9':
        qr_code_url_hash = "{0}|2|{1}|{2}".format(
            chave_nfe, ambiente, int(cid_token))
        c_hash_QR_code = hashlib.sha1((qr_code_url_hash + csc).encode()).hexdigest()
        QR_code_url = "p={}|{}".format(qr_code_url_hash, c_hash_QR_code)
    else:
        qr_code_url_hash = "{0}|2|{1}|{2}|{3}|{4}|{5}".format(
            chave_nfe, ambiente, dia_emissao, valor_total, dig_val, int(cid_token))
        c_hash_QR_code = hashlib.sha1((qr_code_url_hash + csc).encode()).hexdigest()
        QR_code_url = "p={}|{}".format(qr_code_url_hash, c_hash_QR_code)
    vals = {
        'qrcode': qr_code_server + QR_code_url,
        'url_chave': url_chave
    }

    return vals

def add_qr_code(xml_send, qr_code):
    """
    Método usado para adicionar o qr_code e a url de consulta no XML da NFC-e
    :param xml_send: o XML a ser enviado
    :param qr_code: os dados do qrcode e url
    :return: xml com qrcode e url de consulta da NFC-e
    """
    xml = etree.fromstring(xml_send)
    nfe = xml.find(".//{http://www.portalfiscal.inf.br/nfe}NFe")
    qrcode = etree.Element('qrCode')
    url_consulta = etree.Element('urlChave')
    infnfesupl = etree.Element('infNFeSupl')
    qrcode.text = etree.CDATA(qr_code['qrcode'])
    url_consulta.text = qr_code['url_chave']
    infnfesupl.append(qrcode)
    infnfesupl.append(url_consulta)
    nfe.insert(1, infnfesupl)

    return etree.tostring(xml, encoding=str)


def _render(certificado, method, sign, **kwargs):
    path = os.path.join(os.path.dirname(__file__), 'templates')
    xmlElem_send = render_xml(path, '%s.xml' % method, True, **kwargs)

    modelo = xmlElem_send.find(".//{http://www.portalfiscal.inf.br/nfe}mod")
    modelo = modelo.text if modelo is not None else '55'

    if sign:
        signer = Assinatura(certificado.pfx, certificado.password)
        if method == 'NfeInutilizacao':
            xml_send = signer.assina_xml(xmlElem_send, kwargs['obj']['id'])
        if method == 'NfeAutorizacao':
            xml_send = signer.assina_xml(
                xmlElem_send, kwargs['NFes'][0]['infNFe']['Id'])
            if modelo == '65':
                qr_code = generate_qr_code(xml_send, **kwargs)
                xml_send = add_qr_code(xml_send, qr_code)

        elif method == 'RecepcaoEvento':
            xml_send = signer.assina_xml(
                xmlElem_send, kwargs['eventos'][0]['Id'])
        elif method == 'RecepcaoEventoManifesto':
            xml_send = signer.assina_xml(
                xmlElem_send, kwargs['manifesto']['identificador'])

    else:
        xml_send = etree.tostring(xmlElem_send, encoding=str)

    return xml_send


def _send(certificado, method, **kwargs):
    xml_send = kwargs["xml"]
    base_url = localizar_url(
        method,  kwargs['estado'], kwargs['modelo'], kwargs['ambiente'])

    cert, key = extract_cert_and_key_from_pfx(
        certificado.pfx, certificado.password)
    cert, key = save_cert_key(cert, key)

    session = Session()
    session.cert = (cert, key)
    session.verify = False
    transport = Transport(session=session)

    parser = etree.XMLParser(strip_cdata=False)
    xml = etree.fromstring(xml_send, parser=parser)

    client = Client(base_url, transport=transport)

    port = next(iter(client.wsdl.port_types))
    first_operation = [x for x in iter(
        client.wsdl.port_types[port].operations) if "zip" not in x.lower()][0]

    namespaceNFe = xml.find(".//{http://www.portalfiscal.inf.br/nfe}NFe")
    if namespaceNFe is not None:
        namespaceNFe.set('xmlns', 'http://www.portalfiscal.inf.br/nfe')

    with client.settings(raw_response=True):
        response = client.service[first_operation](xml)
        response, obj = sanitize_response(response.text)
        return {
            'sent_xml': xml_send,
            'received_xml': response,
            'object': obj.Body.getchildren()[0]
        }


def xml_autorizar_nfe(certificado, **kwargs):
    _generate_nfe_id(**kwargs)
    return _render(certificado, 'NfeAutorizacao', True, **kwargs)


def autorizar_nfe(certificado, **kwargs):  # Assinar
    if "xml" not in kwargs:
        kwargs['xml'] = xml_autorizar_nfe(certificado, **kwargs)
    return _send(certificado, 'NfeAutorizacao', **kwargs)


def xml_retorno_autorizar_nfe(certificado, **kwargs):
    return _render(certificado, 'NfeRetAutorizacao', False, **kwargs)


def retorno_autorizar_nfe(certificado, **kwargs):
    if "xml" not in kwargs:
        kwargs['xml'] = xml_retorno_autorizar_nfe(certificado, **kwargs)
    return _send(certificado, 'NfeRetAutorizacao', **kwargs)


def xml_recepcao_evento_cancelamento(certificado, **kwargs):  # Assinar
    return _render(certificado, 'RecepcaoEvento', True, **kwargs)


def recepcao_evento_cancelamento(certificado, **kwargs):  # Assinar
    if "xml" not in kwargs:
        kwargs['xml'] = xml_recepcao_evento_cancelamento(certificado, **kwargs)
    return _send(certificado, 'RecepcaoEvento', **kwargs)


def xml_inutilizar_nfe(certificado, **kwargs):
    return _render(certificado, 'NfeInutilizacao', True, **kwargs)


def inutilizar_nfe(certificado, **kwargs):
    if "xml" not in kwargs:
        kwargs['xml'] = xml_inutilizar_nfe(certificado, **kwargs)
    return _send(certificado, 'NfeInutilizacao', **kwargs)


def xml_consultar_protocolo_nfe(certificado, **kwargs):
    return _render(certificado, 'NfeConsultaProtocolo', False, **kwargs)


def consultar_protocolo_nfe(certificado, **kwargs):
    if "xml" not in kwargs:
        kwargs['xml'] = xml_consultar_protocolo_nfe(certificado, **kwargs)
    return _send(certificado, 'NfeConsultaProtocolo', **kwargs)


def xml_nfe_status_servico(certificado, **kwargs):
    return _render(certificado, 'NfeStatusServico', False, **kwargs)


def nfe_status_servico(certificado, **kwargs):
    if "xml" not in kwargs:
        kwargs['xml'] = xml_nfe_status_servico(certificado, **kwargs)
    return _send(certificado, 'NfeStatusServico', **kwargs)


def xml_consulta_cadastro(certificado, **kwargs):
    return _render(certificado, 'NfeConsultaCadastro', False, **kwargs)


def consulta_cadastro(certificado, **kwargs):
    if "xml" not in kwargs:
        kwargs['xml'] = xml_consulta_cadastro(certificado, **kwargs)
        kwargs['modelo'] = '55'
    return _send(certificado, 'NfeConsultaCadastro', **kwargs)


def xml_recepcao_evento_carta_correcao(certificado, **kwargs):  # Assinar
    return _render(certificado, 'RecepcaoEvento', True, **kwargs)


def recepcao_evento_carta_correcao(certificado, **kwargs):  # Assinar
    if "xml" not in kwargs:
        kwargs['xml'] = xml_recepcao_evento_carta_correcao(
            certificado, **kwargs)
    return _send(certificado, 'RecepcaoEvento', **kwargs)


def xml_recepcao_evento_manifesto(certificado, **kwargs):  # Assinar
    return _render(certificado, 'RecepcaoEvento', True, **kwargs)


def recepcao_evento_manifesto(certificado, **kwargs):  # Assinar
    if "xml" not in kwargs:
        kwargs['xml'] = xml_recepcao_evento_manifesto(certificado, **kwargs)
    return _send(certificado, 'RecepcaoEvento', **kwargs)


def xml_consulta_distribuicao_nfe(certificado, **kwargs):  # Assinar
    return _render(certificado, 'NFeDistribuicaoDFe', False, **kwargs)


def _send_v310(certificado, **kwargs):
    xml_send = kwargs["xml"]
    base_url = localizar_url(
        'NFeDistribuicaoDFe',  kwargs['estado'], kwargs['modelo'],
        kwargs['ambiente'])

    cert, key = extract_cert_and_key_from_pfx(
        certificado.pfx, certificado.password)
    cert, key = save_cert_key(cert, key)

    session = Session()
    session.cert = (cert, key)
    session.verify = False
    transport = Transport(session=session)

    xml = etree.fromstring(xml_send)
    xml_um = etree.fromstring('<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/"><cUF>AN</cUF><versaoDados>1.00</versaoDados></nfeCabecMsg>')
    client = Client(base_url, transport=transport)

    port = next(iter(client.wsdl.port_types))
    first_operation = next(iter(client.wsdl.port_types[port].operations))
    with client.settings(raw_response=True):
        response = client.service[first_operation](nfeDadosMsg=xml, _soapheaders=[xml_um])
        response, obj = sanitize_response(response.text)
        return {
            'sent_xml': xml_send,
            'received_xml': response,
            'object': obj.Body.nfeDistDFeInteresseResponse.nfeDistDFeInteresseResult
        }


def consulta_distribuicao_nfe(certificado, **kwargs):
    if "xml" not in kwargs:
        kwargs['xml'] = xml_consulta_distribuicao_nfe(certificado, **kwargs)
    return _send_v310(certificado, **kwargs)


def xml_download_nfe(certificado, **kwargs):  # Assinar
    return _render(certificado, 'NFeDistribuicaoDFe', False, **kwargs)


def download_nfe(certificado, **kwargs):
    if "xml" not in kwargs:
        kwargs['xml'] = xml_download_nfe(certificado, **kwargs)
    return _send_v310(certificado, **kwargs)
