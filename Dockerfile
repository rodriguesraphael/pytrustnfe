FROM python:3.5-stretch
LABEL maintainer="CYBERSIS"

# Generate locale C.UTF-8 for postgres and general locale data
ARG TERM=xterm-color
ARG DEBIAN_FRONTEND=noninteractive
ARG PATH_HOME_USER

RUN cd /usr/lib/apt/methods && \
    ln -s http https

RUN apt-get update -qq &&  \
    apt-get upgrade -qq -y && \
    apt-get -y --no-install-recommends install \
        apt-transport-https \
        lsb-release \
        ca-certificates \
        openssh-server \
        locales \
        locales-all \
        git \
        wget \
        curl \
        sudo \
        nano \
        adduser \
        apt-utils \
	python-libxml2 \
	libxmlsec1-dev \
	python-openssl \
	python-cffi

RUN apt-get autoremove -y && \
    apt-get autoclean